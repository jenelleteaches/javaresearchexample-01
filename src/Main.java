class Researcher {
	String name;
	
	static volatile int researchBudget = 0;
	
	public Researcher() {}
	
	
	public synchronized static void acquireFunding(int amount) {
		researchBudget = researchBudget + amount;
	}
	
	public synchronized static void spendFunding(int amount) {
		
		researchBudget = researchBudget - amount;
		
	}

	// accessor method
	public synchronized static int getBudget() {
		
		return researchBudget;
		
	}
	
}


public class Main {
	public static void main(String[] args) {
		Researcher ed = new Researcher();
		Researcher magdin = new Researcher();
		
		Thread t1 = new Thread("t1") {
			public void run() {
				// put all the code you want the thread to do in here
				System.out.println(this.getName() + "is running");
				for (int i = 0; i< 1000; i++) {
					ed.acquireFunding(1);
				}
			}
		};
		
		Thread t2 = new Thread("t2") {
			public void run() {
				// put all the code you want the thread to do in here
				System.out.println(this.getName() + "is running");
				for (int i = 0; i< 1000; i++) {
					magdin.spendFunding(1);
				}
				
			}
		};
		
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		}
		catch (InterruptedException e) {
			
		}
		
		
		System.out.println("Total budget according to Magin: " + magdin.getBudget());
		System.out.println("Total budget according to Ed: " + ed.getBudget());
		
		
		
		
		
		
//		ed.acquireFunding(100);
//		
//		System.out.println("Reseach budget: " + ed.getBudget());
//		
//		magdin.spendFunding(50);
//		
//		System.out.println("Reseach budget: " + magdin.getBudget());
		
		
		
		
	}
}
